<!DOCTYPE html>
<html>
<head>
	<title>Activity</title>
	<link rel="stylesheet" type="text/css" href="https://bootswatch.com/4/united/bootstrap.css">
</head>
<body>
	<h1 class="text-center my-5">Welcome!</h1>
	<div class="col-lg-4 offset-lg-4">
		<form class="p-4" action="controllers/zodiac-process.php" method="POST">
			<div class="form-group">
				<label class="firstName">Name:</label>
				<input type="text" name="firstName" class="form-control">
			</div>
			<div class="form-group">
				<label for="birthMonth">Birth Month:</label>
				<input type="number" name="birthMonth" class="form-control">
			</div>
			<div class="form-group">
				<label for="birthDay">Birth Day:</label>
				<input type="number" name="birthDay" class="form-control">
			</div>
			<div class="text-center">
				<button class="btn btn-success">Check Zodiac</button>
			</div>


		</form>
	</div>
</body>
</html>